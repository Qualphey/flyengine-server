
const WORLD = global.world;
const Trees = require("./trees/index.js");

const BSON = require("bson");

const fs = require("fs");

module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();
/*
      let table = await aura.table('', {
        columns: {
          id: "uuid PRIMARY KEY DEFAULT gen_random_uuid()",
          name: 'varchar(128)',
          user_id: 'uuid'
        }
      });
        */
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async get_zone(rx, ry, zx, zy, radius) {
    const total_tiles = Math.pow(radius*2+1, 2);

    const force_gen = false;

    const sx = zx-radius;
    const sy = zy-radius;

    const ex = zx+radius+1;
    const ey = zy+radius+1;

        console.log("PARSING ZONE", rx+"-"+ry, zx+"-"+zy, "r"+radius);

    const existing = [];
      
    for (let x = sx; x < ex; x++) {
      for (let y = sy; y < ey; y++) {
        let nrzxy = this.adapt_coordinates(rx, ry, x, y, 10);


        let ztable = await WORLD.aura.table('environment_zone_'+nrzxy.rx+'_'+nrzxy.ry+'_'+nrzxy.zx+'_'+nrzxy.zy, {
          columns: {
            id: "uuid PRIMARY KEY DEFAULT gen_random_uuid()",
            json: "jsonb"
          }
        }); // 
        let generate = force_gen;
        if (force_gen) {
          await ztable.delete();
        } else {
          const exis = await ztable.select("*");
          if (exis.length == 0) {
            generate = true;
          } else {
            for (let e = 0; e < exis.length; e++) {
              exis[e].json.dx = x-zx;
              exis[e].json.dy = y-zy;
              existing.push(exis[e]);
            }
          }
        }


        if (generate) {
          let zone = BSON.deserialize(new Uint8Array(fs.readFileSync(WORLD.app_path+"/envmap/region-"+nrzxy.rx+"_"+nrzxy.ry+"/zone-"+nrzxy.zx+"_"+nrzxy.zy+".bson")));
          let height = undefined;
          let width = height = Math.sqrt(zone.data.buffer.length);
          for (let i = 0; i < zone.data.buffer.length; i++) {
            let item = zone.data.buffer[i];

            let new_object = {
              type: item,
              x: i%width,
              y:Math.floor(i/width)
            }

            switch(item) {
              case 0:
                new_object = false;
                break;
              case 1:
                new_object.age = Math.round(Math.random()*3);
                break;
              case 2:

                break;
              case 3:

                break;
              case 4:

                break;
              default:
                console.error(new Error("INVALID ENVIRONMENT ITEM", item));
            }

            if (new_object) {
              new_object.dx = x-zx;
              new_object.dy = y-zy;
              const oobj = {
                json: new_object
              };
              new_object.id = (await ztable.query(
                "INSERT INTO "+ztable.name+"(json) VALUES ('"+JSON.stringify(oobj.json)+"'::jsonb) RETURNING id;"
              )).rows[0].id;
              existing.push(oobj);
            }
          }
        }

      }
    }

    return existing;
  }

  adapt_coordinates(rx, ry, zx, zy, region_size) {
    while (zx < 0) {
      rx--;
      zx += region_size;
    }
    while (zx >= region_size) {
      rx++;
      zx -= region_size;
    }
    while (zy < 0) {
      ry--;
      zy += region_size;
    }
    while (zy >= region_size) {
      ry++;
      zy -= region_size;
    }
    return {
      rx: rx,
      ry: ry,
      zx: zx,
      zy: zy
    }
  }

  constructor() {

  }
}


