'use strict'

const ObjectId = GLOBAL.mongoose.Schema.Types.ObjectId;

const StructureSchema = new GLOBAL.mongoose.Schema({
  name : String,
  lvl : { type: Number },
  x : { type: Number },
  y : { type: Number },
  load : [Number],
  owner : ObjectId
});

global.db_models.Structure = GLOBAL.mongoose.model('Structure', StructureSchema, 'Structures');

module.exports = class {
  constructor(doc) {
    this.name = doc.name;
    this.lvl = doc.lvl;
    this.x = doc.x;
    this.y = doc.y;
    this.load = doc.load;
    this.owner = doc.owner;
    this.id = doc._id.toString();
  }

  prepare_data() {
    this.doc = {
      name : this.name,
      lvl : this.lvl,
      x : this.x,
      y : this.y,
      load : this.load,
      owner : this.owner,
      id : this.id
    }
    return this.doc;
  }
}
