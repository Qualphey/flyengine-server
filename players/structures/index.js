

module.exports = class {
  static async init(aura, player) {
    try {
      let table = await aura.table("structures", {
        columns: {
          id: "uuid",
          name: "varchar(128)",
          lvl: "integer",
          x: "numeric",
          y: "numeric",
          angle: "numeric",
          owner: "UUID REFERENCES players(id)",
          config: "jsonb",
          properties: "jsonb",
          grid_tiles: "jsonb"
        }
      });

      return new module.exports(aura, table, player);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(aura, table, player) {
    this.table = table;
  }

}
