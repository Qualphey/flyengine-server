const fs = require("fs");
const path = require("path");

module.exports = class {
  static async init(aura, player) {
    try {
      let table = await aura.table("constructions", {
        columns: {
          id: "uuid",
          name: "varchar(128)",
          lvl: "integer",
          x: "numeric",
          y: "numeric",
          angle: "numeric",
          res_available: "JSONB",
          res_used: "JSONB",
          owner: "UUID REFERENCES players(id)",
          grid_tiles: "jsonb"     
        }
      });

      return new module.exports(table, player);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(table, player) {
    this.config = {};
    let this_class = this;
    fs.readdirSync(path.resolve(__dirname, 'config')).forEach(file => {
      if (file.endsWith('.json')) {
        this_class.config[file.slice(0, -5)] = JSON.parse(fs.readFileSync(path.resolve(__dirname, "config/"+file)));   
      } else {
   
      }
    });
    console.log("CONSTRUCTIONS CONFIG", this.config);
    this.table = table
    this.player = player
  }

  async add(data) {
    try {
      if (data.what && data.pos && data.angle) {
        let null_res = {}
        let cost = this.config[data.what].lvls[data.lvl].cost;
        for (let res_type in cost) {
          null_res[res_type] = 0;
        }
        let construct = {
          name: data.what,
          lvl: data.lvl,
          x: data.pos.x,
          y: data.pos.z,
          angle: data.angle,
          res_available: null_res,
          res_used: null_res,
          owner: this.player.cfg.id,
          grid_tiles: data.grid_tiles
        }
        construct.id = await this.table.insert(construct);
        return construct;
      }
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async remove(data) {
    try {
      await this.table.delete("id = $1", [data.construction_id]);
      return data;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

}
