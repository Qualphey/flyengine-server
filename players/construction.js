'use strict'

const ObjectId = GLOBAL.mongoose.Schema.Types.ObjectId;

const ConstructionSchema = new GLOBAL.mongoose.Schema({
  name : String,
  lvl : { type: Number },
  x : { type: Number },
  y : { type: Number },
  workers : [ObjectId],
  build_points : { type: Number },
  start_time : { type: Date },
  finish_time : { type: Date },
  owner : ObjectId
});

global.db_models.Construction = GLOBAL.mongoose.model('Construction', ConstructionSchema, 'Constructions');

module.exports = class {
  constructor(doc) {

    this.id = doc._id.toString();
    this.doc = doc;
  }

  prepare_data() {
    this.data = {
      name : this.doc.name,
      lvl : this.doc.lvl,
      x : this.doc.x,
      y : this.doc.y,
      workers : this.doc.workers,
      build_points : this.doc.build_points,
      start_time : this.doc.start_time,
      finish_time : this.doc.finish_time,
      owner : this.doc.owner,
      id : this.id
    }
    return this.data;
  }
}
