'use strict'

const ObjectId = GLOBAL.mongoose.Schema.Types.ObjectId;

const ResourceTransportSchema = new GLOBAL.mongoose.Schema({
    model : String,
    lvl : { type: Number },
    x : { type: Number },
    y : { type: Number },
    dest_x : { type: Number },
    dest_y : { type: Number },
    start_time : { type: Date },
    dest_time : { type: Date },
    dest_struct : ObjectId,
    load : [Number],
    traveling : Boolean,
    owner : ObjectId
});

global.db_models.ResourceTransport = GLOBAL.mongoose.model('ResourceTransport', ResourceTransportSchema, 'Units');

const Unit = require('./unit.js');

module.exports = class extends Unit {
  constructor(doc) {
    super(doc);
  }

  move(where, enter, next) {
    if (enter) {
      super.move(where, function() {

      });
    } else {
      super.move(where);
    }
  }

  group(group_id, next) {
    var this_class = this;
    global.db_models.Group.findById(group_id, function(err, group) {
      if (err) { console.log(err); };
      group.units.push(this_class.id);

      group.save(function(err) {
        if (err) { console.log(err) }
        global.db_models.ResourceTransport.findById(this_class.id, function(err, res_transp) {
          if (err) { console.log(err); };
          res_transp.traveling = true;
          res_transp.save(function(err) {
            if (err) { console.log(err) }
            if (next) {
              next();
            }
          });
        });
      });
    });
  }

  static find_by_owner(owner_id, next) {
    global.db_models.ResourceTransport.find(
      { model: "resource_transport", owner: owner_id },
      function(err, list) {
        if (err) { console.log(err); };
        next(list);
      }
    );
  }

  static create(owner_id, x, y, next) {
    var res_transp = new global.db_models.ResourceTransport({
      model: "resource_transport",
      owner: owner_id,
      x: distortFloat(x),
      y: distortFloat(y)
    });

    res_transp.save(function(err) {
      if (err) { console.log(err); };

      next(res_transp._doc);
    });
  }
}

function randFloat(min, max) {
  return Math.random() * (max - min) + min;
}

function distortFloat(value) {
  var volume = 300;
  var min = -(volume/2);
  var max = (volume/2);
  var distortion = randFloat(max, min);
  return value+distortion;
}
