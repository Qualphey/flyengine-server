'use strict'

const ObjectId = GLOBAL.mongoose.Schema.Types.ObjectId;

const WorkerCrewSchema = new GLOBAL.mongoose.Schema({
    model : String,
    count : { type: Number },
    x : { type: Number },
    y : { type: Number },
    dest_x : { type: Number },
    dest_y : { type: Number },
    start_time : { type: Date },
    dest_time : { type: Date },
    traveling : Boolean,
    owner : ObjectId
});

global.db_models.WorkerCrew = GLOBAL.mongoose.model('WorkerCrew', WorkerCrewSchema, 'Units');

var Vec2 = require('vec2');

const Unit = require('./unit.js');

module.exports = class extends Unit {
  constructor(doc) {
    super(doc);
  }


  static async init() {
    try {

    } catch (e) {
      console.error(e.stack);
    }
  }



  move(where, enter, next) {
    if (enter) {
      super.move(where, function() {
        if (enter.doc.workers) {
          next("worker_crew");
        }
      });
    } else {
      super.move(where);
    }
  }

  prepare_data() {
    var data = super.prepare_data();
    data.count = this.doc.count;
    return data;
  }

  group(group_id, next) {
    var this_class = this;
    global.db_models.Group.findById(group_id, function(err, group) {
      if (err) { console.log(err); };
      group.units.push(this_class.id);

      group.save(function(err) {
        if (err) { console.log(err) }
        global.db_models.WorkerCrew.findById(this_class.id, function(err, worker) {
          if (err) { console.log(err); };
          worker.traveling = true;
          worker.save(function(err) {
            if (err) { console.log(err) }
            if (next) {
              next();
            }
          });
        });
      });
    });
  }

  static find_by_owner(owner_id, next) {
    global.db_models.WorkerCrew.find(
      { model: "worker_crew", owner: owner_id },
      function(err, list) {
        if (err) { console.log(err); };
        next(list);
      }
    );
  }

  static create(owner_id, x, y, next) {
    var wo_crew = new global.db_models.WorkerCrew({
      model: "worker_crew",
      owner: owner_id,
      x: distortFloat(x),
      y: distortFloat(y)
    });

    wo_crew.save(function(err) {
      if (err) { console.log(err); };

      next(wo_crew._doc);
    });
  }
}

function randFloat(min, max) {
  return Math.random() * (max - min) + min;
}

function distortFloat(value) {
  var volume = 300;
  var min = -(volume/2);
  var max = (volume/2);
  var distortion = randFloat(max, min);
  return value+distortion;
}
