'use strict'

var Vec2 = require('vec2');
/**
 * @class Unit
 * @arg {Object} cfg
 */
module.exports = class {
  constructor(cfg) {
    this.id = cfg.id;

    this.cfg = cfg;

    this.move_timeout = false;

    this.speed = 25; // SPEED
  }

  static async init(cfg) {
    try {
      return new module.exports(cfg);
    } catch (e) {
      console.error(e.stack);
    }
  }


  move(where) {
    var start_time = Date.now();
    var path_vec = { x: this.doc.x-where.x, y: this.doc.y-where.y }
    var distance = Math.sqrt( path_vec.x*path_vec.x + path_vec.y*path_vec.y );
    var time_left = (distance/this.speed)*1000;
    var dest_time = start_time+time_left;

    if (this.move_timeout) {
      clearTimeout(this.move_timeout);

      var path_vec = new Vec2(
        this.doc.dest_x - this.doc.x,
        this.doc.dest_y - this.doc.y
      );

      var total_time =  this.doc.dest_time - this.doc.start_time;
      var passed_time = start_time - this.doc.start_time;

      var distance = path_vec.length();
      var passed_distance = passed_time * distance / total_time;

      var norm_vec = path_vec.normalize(true);
      var dir_vec = new Vec2(
        norm_vec.x*passed_distance,
        norm_vec.y*passed_distance
      );

      this.doc.x = this.doc.x+dir_vec.x;
      this.doc.y = this.doc.y+dir_vec.y;
      this.doc.save(function(err) {
        if (err) { console.log(err); }
      });
    }

    var this_class = this;

    this.move_timeout = setTimeout(() => {
      this_class.doc.x = where.x;
      this_class.doc.y = where.y;
      this_class.doc.save(function(err) {
        if (err) { console.log(err); }
        if (next) {
          next();
        }
      });
    }, time_left);

    this.doc.dest_x = where.x;
    this.doc.dest_y = where.y;
    this.doc.start_time = start_time;
    this.doc.dest_time = dest_time;
    this.doc.save(function(err) {
      if (err) { console.log(err); }
      global.io.sockets.emit('worldmod.io-event', {
        event: "unit_move",
        start_pos: { x: this_class.doc.x, y: this_class.doc.y },
        dest_pos: where,
        start_time: start_time,
        dest_time: dest_time,
        id: this_class.id
      });
    });
  }

  prepare_data() {
    this.data = {
      model : this.doc.model,
      x : this.doc.x,
      y : this.doc.y,
      dest_x : this.doc.dest_x,
      dest_y : this.doc.dest_y,
      start_time : this.doc.start_time,
      dest_time : this.doc.dest_time,
      owner : this.doc.owner,
      id : this.id
    }
    return this.data;
  }
}
