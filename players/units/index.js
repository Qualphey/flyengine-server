'use strict'

//const ResourceTransport = require('./resource_transport.js');
//const WorkerCrew = require('./worker_crew.js');
//const Troop = require('./troop.js');

const Unit = require('./unit.js');
let Vec2 = require("vec2");

const wholewa = 26;
const territorya = 50;
const tilea = 300;

const fs = require("fs");
const path = require("path");
const items_config = JSON.parse(fs.readFileSync(path.resolve(__dirname, "items/config.json")));

const world_width = wholewa*territorya*tilea, world_height = wholewa*territorya*tilea;

/**
 * @class Units
 * @hidecontructor
 */
module.exports = class {
  constructor(aura, table, player) {
    this.aura = aura;
    this.table = table;
    this.player = player;

    this.units = [];
   
/*    this.list = [];
    this.list_docs = [];

    this.components_loaded = 0;

    this.owner = owner_id;

    var this_class = this;

    WorkerCrew.find_by_owner(owner_id, function(list) {
      ResourceTransport.find_by_owner(owner_id, function(list1) {
        list.push.apply(list, list1);
        Troop.find_by_owner(owner_id, function(list2) {
          list.push.apply(list, list2);
          for (var u = 0; u < list.length; u++) {
            var un = list[u];
            switch (un.model) {
              case "resource_transport":
                var unit = new ResourceTransport(un);
                this_class.list.push(unit);
                break;
              case "worker_crew":
                var unit = new WorkerCrew(un);
                this_class.list.push(unit);
                break;
              case "troop":
                var unit = new Troop(un);
                this_class.list.push(unit);
                break;
              default:
                console.log("Invalid unit type:", un.model);
            }

          }
          this_class.components_loaded++;
        });
      });
    });

    function ckComponents() {
      if (this_class.components_loaded == 1) {
        this_class.prepare_data();
        next();
      } else {
        setTimeout(ckComponents, 500);
      }
    }
    ckComponents();*/
  }

  /**
   * @statis
   * @async
   * @method init
   * @memberof Units
   * @arg {Aura} aura
   * @arg {Player} player
   * @returns {Units}
   */
  static async init(aura, player) {
    try {
      let table = await aura.table("units", {
        columns: {
          id: "uuid",
          name: "varchar(128)",
          lvl: "integer",
          x : "numeric",
          y : "numeric",
          dest_x : "numeric",
          dest_y : "numeric",
          path : "JSONB",
          start_time : "bigint",
          dest_time : "bigint",
          command : "varchar(64)",
          interaction: "varchar(128)",
          inventory: "JSONB",
  //        dest_struct : ObjectId,
  //        caravan : ObjectId,
  //        load : [Number],
          traveling : "boolean",
          owner: "UUID REFERENCES players(id)"         
        }
      });

      return new module.exports(aura, table, player);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
  /**
   * @async
   * @method move
   * @memberof Units
   * @arg {Object} data command configuration
   * @arg {String} data.id objects id in database
   * @arg {Integer} data.x coordinate of the destination point
   * @arg {Integer} data.y coordinate of the destination point
   * @arg {Object} data.path path for the unit to move on
   * @arg {Object[]} data.path.steps destination step coordinates to avoid obstacles i.e. [{x: 123, y: -123}, {x: -123, y: 123}]
   * @arg {Object} data.path.start starting location in x,y plane for the unit to move from
   * @arg {Integer} data.path.start.x x coordinate of the starting location
   * @arg {Integer} data.path.start.y y coordinate of the starting location
   * @returns {Object} updated unit object
   */
  async move(data) {
    try {
      let cur_pos = (await this.table.select("*", "id = $1", [data.id]))[0];

      console.log("cur_pos",cur_pos);
      console.log("data", data);
        


      let time_now = Date.now();

      let upvalues = {
        x: data.path.start.x,
        y: data.path.start.y,
        dest_x : data.x,
        dest_y : data.y,
        start_time : time_now,
        command: data.command,
        interaction: ""
      }

      if (cur_pos.dest_time) {
        if (Date.now() > cur_pos.dest_time) {
          upvalues.x = parseFloat(cur_pos.dest_x),
          upvalues.y = parseFloat(cur_pos.dest_y)
        } else {
          let cur_path = cur_pos.path;
          for (let s = 0; s < cur_path.steps.length; s++) {
            let step = cur_path.steps[s];
            if (time_now < step.time) {
              let s_time = cur_path.start.time;
              let sx = cur_path.start.x, sy = cur_path.start.y;
              if (s > 0) {
                s_time = cur_path.steps[s-1].time;
                sx = cur_path.steps[s-1].x;
                sy = cur_path.steps[s-1].y;
              }

              var passed_time = time_now - s_time;
              var distance = step.length;

              let total_time =  step.time - s_time;
              var passed_distance = passed_time * distance / total_time;

              var dir_vec = (new Vec2(
                step.x-sx, step.y-sy
              )).divide(distance).multiply(passed_distance);//move.segment.set_length(passed_distance);
              console.log("curp - dir_vec", dir_vec);

              upvalues.x = sx+dir_vec.x;
              upvalues.y = sy+dir_vec.y;


              break;
            }
          }
 /*         let move = (function (start_pos, dest_pos, start_time, dest_time) {
            return {
              A : start_pos,
              B : dest_pos,
              segment : {
                x: dest_pos.x - start_pos.x,
                y: dest_pos.y - start_pos.y,
                length: function() {
                  return Math.sqrt(this.x*this.x+this.y+this.y);
                },
                set_length: function(max) {
                  let dx = this.x, dy = this.y
                  let d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
                  if (d > max) {
                    this.x = start_pos.x + max*dx/d;
                    this.y = start_pos.y + max*dy/d;
                    d = max;
                  }
                  return this;
                }
              },
              tegment: new Vec2(dest_pos.x - start_pos.x, dest_pos.y - start_pos.y),
              tA : start_time,
              tB : dest_time,
              t : dest_time - start_time
            }
          })(
            { x: parseFloat(cur_pos.x), y: parseFloat(cur_pos.y) },
            { x: parseFloat(cur_pos.dest_x), y: parseFloat(cur_pos.dest_y) },
            parseInt(cur_pos.start_time), parseInt(cur_pos.dest_time)
          );
          var passed_time = time_now - move.tA;
          var distance = move.tegment.length()//move.segment.length();
          var passed_distance = passed_time * distance / move.t;

          var dir_vec = move.tegment.divide(distance).multiply(passed_distance);//move.segment.set_length(passed_distance);
          console.log("curp - dir_vec", dir_vec);

          upvalues.x = parseFloat(cur_pos.x)+dir_vec.x;
          upvalues.y = parseFloat(cur_pos.y)+dir_vec.y;
*/
        }
      }
      
      data.path.start = {
        x: upvalues.x,
        y: upvalues.y,
        time: time_now
      };
      let path = data.path;

      let total_distance = 0;
      console.log("path.start", path.start);
      let sx = path.start.x, sy = path.start.y, stime = path.start.time;
      for (let s = 0; s < data.path.steps.length; s++) {
        let step = data.path.steps[s];
      console.log("step", step);
      console.log(sx, sy);
        let a = sx - step.x;
        var b = sy - step.y;
        let s_dist = Math.sqrt( a*a + b*b );
        total_distance += s_dist;
      console.log("s_dist", s_dist);
        stime += s_dist//*20;
        step.time = stime;
        step.length = s_dist;
        sx = step.x; sy = step.y;
      }

      let last_step = path.steps[path.steps.length-1];
      let lsa = last_step.x - path.dest.x;
      var lsb = last_step.y - path.dest.y;
      let ls_length = Math.sqrt( lsa*lsa + lsb*lsb );
      total_distance += ls_length;

      path.length = total_distance;
      path.dest.time = time_now+total_distance//*20;



      path.steps.push({
        x: path.dest.x,
        y: path.dest.y,
        time: path.dest.time,
        length: ls_length
      });


      upvalues.path = path;

      let adistance = total_distance;
      upvalues.dest_time = time_now+adistance//*20;
      console.log("upvalues.dest_time", upvalues.dest_time);

      await this.table.update(upvalues, "id = $1", [data.id]);

      upvalues.id = data.id;
      let cpval = JSON.parse(JSON.stringify(upvalues));
      cpval.action = data.command;
      cpval.command = "unit_move";
      cpval.what = data.what;

      this.player.socket.emit("world.io-command_response", cpval);

      return upvalues;
    } catch(e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async verify_command(data) {
    try {
      let cur_state = data.cur_state = (await this.table.select("*", "id = $1", [data.id]))[0];
      let this_class = this;
      if (cur_state && data.command == cur_state.command &&
          data.x == cur_state.dest_x && data.y == cur_state.dest_y) {
          switch (data.command) {
            case "unit_gather":
              await this_class.gather(data);
              break;
            case "unit_bringres":
              await this_class.bringres(data);
              break;
            case "unit_construct":
              await this_class.construct(data);
              break;
            case "unit_store_res":
              await this_class.store_res(data);
              break;
            default:
              console.error(new Error("Unknown world.ui-command :", data.command));
          }
       }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async gather(data) {
    try {
      let interaction = "gather@"+data.what;
      await this.table.update({
        interaction: interaction
      }, "id = $1", [data.id]);

      let this_class = this;
      let cur_state = (await this.table.select("*", "id = $1", [data.id]))[0];

      let item;
      if (data.what == "forest") {
        item = "wood";
      } else if (data.what == "stones") {
        item = "stone";
      } else if (data.what == "iron") {
        item = "iron";
      }

      if (this.inv_space(item, cur_state)) {
        setTimeout(async () => {
          try {
            cur_state = (await this_class.table.select("*", "id = $1", [data.id]))[0];
            if (cur_state && data.command == cur_state.command && interaction == cur_state.interaction &&
                data.x == cur_state.dest_x && data.y == cur_state.dest_y) {

              if (await this_class.stash(item, cur_state)) {
                let response = {
                  command: data.command,
                  id: data.id,
                  what: item
                };
                this_class.player.socket.emit("world.io-command_response", response);
                await this_class.gather(data);
              }
            }
          } catch (e) {
            console.error(e.stack);
          }
        }, Math.floor(Math.random() * (3000 - 1000 + 1)) + 1000)
      } else {
        let response = {
          command: data.command,
          id: data.id,
          err: "inv_full"
        };
        this.player.socket.emit("world.io-command_response", response);

      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  inv_space(what, cur_state) {
    let inv = cur_state.inventory;
    if (!inv || inv == null) inv = {};
    if (!inv[what]) {
      inv[what] = 1;
    } else {
      inv[what]++;
    }

    let total_mass = 0;
    for (let i in inv) {
      total_mass += inv[i]*items_config[i].mass;
    }
    console.log("total_mass", total_mass);

    if (total_mass <= 50) {
      return true;
    } else {
      console.log("CANNOT CARRY ANY MORE STUFF");
      return false;
    }
  }

  async stash(what, cur_state) {
    try {
      let inv = cur_state.inventory;
      if (!inv || inv == null) inv = {};
      if (!inv[what]) {
        inv[what] = 1;
      } else {
        inv[what]++;
      }

      await this.table.update({
        inventory: inv
      }, "id = $1", [cur_state.id]);
      return true;
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async bringres(data) {
    try {
      let target = (await this.player.constructions.table.select("*", "id = $1", [data.construction_id]))[0];

      const costs = this.player.constructions.config[target.name].lvls[target.lvl].cost;
      for (let item in costs) {
        let amount = data.cur_state.inventory[item] || 0;
        let cost = costs[item];
        let used = target.res_used[item];
        let available = target.res_available[item];
        console.log(item, amount, cost, used, available);

        if (amount > cost-used-available) {
          let left = amount-cost+used+available;
          let inv = data.cur_state.inventory;
          inv[item] = left;
          await this.table.update({
            inventory: inv
          }, "id = $1", [data.id]);

          let res_available = target.res_available;
          res_available[item] = cost-used;
          await this.player.constructions.table.update({
            res_available: res_available
          }, "id = $1", [data.construction_id]);

          let response = {
            command: data.command,
            unit_id: data.id,
            unit_inv: inv,
            construct_id: data.construction_id,
            construct_res_aval: res_available
          };
          this.player.socket.emit("world.io-command_response", response);
        } else {
          let inv = data.cur_state.inventory;
          delete inv[item];
          await this.table.update({
            inventory: inv
          }, "id = $1", [data.id]);

          let res_available = target.res_available;
          res_available[item] += amount;
          await this.player.constructions.table.update({
            res_available: res_available
          }, "id = $1", [data.construction_id]);

          let response = {
            command: data.command,
            unit_id: data.id,
            unit_inv: inv,
            construct_id: data.construction_id,
            construct_res_aval: res_available
          };
          this.player.socket.emit("world.io-command_response", response);
        }

      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async store_res(data) {
    let target = (await this.player.structures.table.select("*", "id = $1", [data.structure_id]))[0];
    let nres = {};
    if (target.properties && target.properties.resources) {
      for (let tkey in target.properties.resources) {
        nres[tkey] = target.properties.resources[tkey]
      }
    }
    console.log("NRES", nres);
    for (let key in data.resources) {
      if (nres[key]) {
        nres[key] += data.resources[key]
      } else {
        nres[key] = data.resources[key]
      }
    }

    if (target.properties && typeof target.properties === 'object' && target.properties.constructor === Object) {
      target.properties.resources = nres;
    } else {
      target.properties = {
        resources: nres
      }
    }

    await this.player.structures.table.update({
      properties: target.properties
    }, "id = $1", [data.structure_id]);
    await this.table.update({
      inventory: {}
    }, "id = $1", [data.unit_id]);
    this.player.socket.emit("world.io-command_response", {
      command: data.command,
      unit_id: data.unit_id,
      structure_id: data.structure_id,
      unit_inv: {},
      structure_res: nres,
      cool: true
    });

  }

  async construct(data) {
    try {
      let interaction = "build@"+data.construction_id;
      await this.table.update({
        interaction: interaction
      }, "id = $1", [data.id]);

      let this_class = this;
      
      let target = (await this.player.constructions.table.select("*", "id = $1", [data.construction_id]))[0];
      let costs = this_class.player.constructions.config[target.name].lvls[target.lvl].cost;
      let finished = true;
      let nores = true;
      for (let i in target.res_available) {
        if (target.res_used[i] < costs[i]) {
          finished = false;
          if (target.res_available[i] > 0) {
            target.res_used[i]++;
            target.res_available[i]--;
            nores = false;
            break;
          }
        }
      }

      if (finished) nores = false;

      if (nores) {
        let response = {
          command: data.command,
          unit_id: data.id,
          construct_id: data.construction_id,
          res_used: target.res_used,
          res_available: target.res_available,
          err: "no_res"
        };
        this_class.player.socket.emit("world.io-command_response", response);
      } else {
        setTimeout(async () => {
          try {
            let cur_state = (await this_class.table.select("*", "id = $1", [data.id]))[0];
            if (cur_state && data.command == cur_state.command && interaction == cur_state.interaction &&
                data.x == cur_state.dest_x && data.y == cur_state.dest_y) {
            


              let response = {
                command: data.command,
                unit_id: data.id,
                construct_id: data.construction_id,
                res_used: target.res_used,
                res_available: target.res_available
              };

              if (!finished) {
                await this_class.player.constructions.table.update({
                  res_used: target.res_used,
                  res_available: target.res_available

                }, "id = $1", [data.construction_id]);
                
                await this_class.construct(data);
              } else {
                let nstruct = {
                  name: target.name,
                  lvl: target.lvl,
                  x: target.x,
                  y: target.y,
                  angle: target.angle,
                  owner: target.owner,
                  config: this_class.player.constructions.config[target.name],
                  grid_tiles: target.grid_tiles
                };
                nstruct.id = await this_class.player.structures.table.insert(nstruct);
                
                response.finished = nstruct;

                await this.player.constructions.table.delete(
                  "id = $1", [data.construction_id]
                );
              }
              
              this_class.player.socket.emit("world.io-command_response", response);
            }
          } catch (e) {
            console.error(e.stack);
          }
        }, Math.floor(Math.random() * (3000 - 1000 + 1)) + 1000)
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async drop(data) {
    try {
      let cur_state = (await this.table.select("*", "id = $1", [data.id]))[0];
      delete cur_state.inventory[data.what];
      await this.table.update({
        inventory: cur_state.inventory
      }, "id = $1", [data.id]);
      data.ninv = cur_state.inventory;
      this.player.socket.emit("world.io-command_response", data);
    } catch (e) {
      console.error(e.stack);
    }
  }

  findById(id) {
    var found = false;
    for (var u = 0; u < this.list.length; u++) {
      if (id === this.list[u].id) {
        found = this.list[u];
      }
    }
    return found;
  }

  async initial(owner) {
    try { 
      let minw = -world_width/2;
      let maxw = world_width/2;
      let minh = -world_height/2;
      let maxh = world_height/2;

      let unit_cfg = {
        name: "warrior",
        lvl: 1,
        x: randFloat(maxw, minw),
        y: randFloat(maxh, minh),
        inventory: {},
        owner: this.player.cfg.id
      }

      unit_cfg.id = await this.table.insert(unit_cfg);
      this.units.push(await Unit.init(unit_cfg));
      
    } catch(e) {
      console.error(e.stack);
    }
/*    var this_class = this;

    var min = -(Math.sqrt(global.world.width)/2)*100;
    var max = (Math.sqrt(global.world.width)/2)*100;

    Group.create(owner, randFloat(max, min), randFloat(max, min), function(doc_group) {
      var group = new Group(doc_group);

      var start_x = randFloat(max, min);
      var start_y = randFloat(max, min);

      var res_transp = new global.db_models.ResourceTransport({
        model: "resource_transport",
        lvl: 0,
        owner: owner,
        x: distortFloat(start_x),
        y: distortFloat(start_y)
      });

      ResourceTransport.create(owner, start_x, start_y, function(doc) {
        var resource_transport = new ResourceTransport(doc);
        resource_transport.group(group.id);
        this_class.list.push(resource_transport);

        WorkerCrew.create(owner, start_x, start_y, function(docw) {
          var worker_crew = new WorkerCrew(docw);
          worker_crew.group(group.id);
          this_class.list.push(worker_crew);

          Troop.create(owner, start_x, start_y, function(doct) {
            var troop = new Troop(doct);
            troop.group(group.id);
            this_class.list.push(troop);
            this_class.list_docs.push(troop.prepare_data());
          });
        });
      });
    });*/

    function randFloat(min, max) {
      return Math.random() * (max - min) + min;
    }

    function distortFloat(value) {
      var volume = 300;
      var min = -(volume/2);
      var max = (volume/2);
      var distortion = randFloat(max, min);
      return value+distortion;
    }
  }

  prepare_data() {
    this.list_docs = [];
    for (var u = 0; u < this.list.length; u++) {
      this.list_docs.push(this.list[u].prepare_data());
    }
    return this.list_docs;
  }
}
