
const Player = require("./player.js");

/**
 * Class handling all players
 * @class Players
 * @hideconstructor
 */
module.exports = class {
  constructor(aura, table) {
    this.players = [];
    this.table = table;
    this.aura = aura;
  }

  static async init(aura) {
    let table = await aura.table('players', {
      columns: {
        id: "uuid PRIMARY KEY DEFAULT gen_random_uuid()",
        name: 'varchar(128)',
        user_id: 'uuid'
      }
    });

    let this_class = new module.exports(aura, table);

    let players = await table.select("*");
    for (let p = 0; p < players.length; p++) {
      this_class.players.push(await Player.init(aura, table, players[p]));
    }

    return this_class;
  }

  async connect(socket) {
    try { 
      let player = undefined;
      for (let p = 0; p < this.players.length; p++) {

        if (this.players[p].cfg.user_id == socket.user_id) {
          console.log("connect", this.players[p].cfg);
          player = this.players[p];
          player.connect(socket);
          break;
        }
      }
      if (!player) {
        player = await Player.init(this.aura, this.table, { user_id: socket.user_id });
        this.players.push(player);
        player.connect(socket);
      }
    } catch(e) {
      console.error(e.stack);
      return undefined;
    }
  }

  async disconnect(socket) {
    try {
      socket.player = undefined;
      return true;
    } catch(e) {
      console.error(e.stack);
      return false;
    }
  }
}
