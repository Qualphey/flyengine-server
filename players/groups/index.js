'use strict'

const Group = require('./group.js');

module.exports = class {
  constructor(owner_id, next) {
    this.list = [];
    this.list_docs = [];

    this.components_loaded = 0;

    this.owner = owner_id;

    var this_class = this;

    Group.find_by_owner(owner_id, function(list) {
      for (var u = 0; u < list.length; u++) {
        var group = new Group(list[u]);
        this_class.list.push(group);
      }
    });

    function ckComponents() {
      if (this_class.components_loaded == 1) {
        this_class.prepare_data();
        next();
      } else {
        setTimeout(ckComponents, 500);
      }
    }
    ckComponents();
  }

  findById(id) {
    var found = false;
    for (var u = 0; u < this.list.length; u++) {
      if (id === this.list[u].id) {
        found = this.list[u];
      }
    }
    return found;
  }

  prepare_data() {
    this.list_docs = [];
    for (var u = 0; u < this.list.length; u++) {
      this.list_docs.push(this.list[u].prepare_data());
    }
    return this.list_docs;
  }
}
