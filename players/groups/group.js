'use strict'

const ObjectId = GLOBAL.mongoose.Schema.Types.ObjectId;

const GroupSchema = new GLOBAL.mongoose.Schema({
  x : { type: Number },
  y : { type: Number },
  dest_x : { type: Number },
  dest_y : { type: Number },
  start_time : { type: Date },
  dest_time : { type: Date },
  dest_struct : ObjectId,
  units : [ObjectId],
  owner : ObjectId,
  model : String
});

global.db_models.Group = GLOBAL.mongoose.model('Group', GroupSchema, 'Groups');

const Unit = require('../units/unit.js');

module.exports = class extends Unit {
  constructor(doc) {
    super(doc);
  }

  move(where, enter, next) {
    if (enter) {
      super.move(where, function() {

      });
    } else {
      super.move(where);
    }
  }

  static find_by_owner(owner_id, next) {
    global.db_models.Group.find(
      { owner: owner_id },
      function(err, list) {
        if (err) { console.log(err); };
        next(list);
      }
    );
  }

  static create(owner_id, x, y, next) {
    var group = new global.db_models.Group({
      name: "group",
      owner: owner_id,
      x: x,
      y: y
    });

    group.save(function(err) {
      if (err) { console.log(err); };

      next(group);
    });
  }
}
