

let Units = require('./units/index.js');
//var Groups = require('./groups/index.js');
const Structures = require('./structures/index.js');
const Constructions = require('./structures/constructions.js');

const fs = require('fs');
const path = require('path');

let Vec2 = require("vec2");

const WORLD = global.world;

/**
 * @class Player
 * @hidecontructor
 */
module.exports = class {
  /**
   * @static
   * @async
   * @method init
   * @memberof Player
   * @arg {Aura} aura
   * @arg {Aura#Table} table
   * @arg {Object} cfg
   * @returns {Player}
   */
  static async init(aura, table, cfg) {
    try {
      let this_class = new module.exports(table, cfg);

      this_class.constructions = await Constructions.init(aura, this_class);
      this_class.structures = await Structures.init(aura, this_class);
      this_class.units = await Units.init(aura, this_class);

      return this_class;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(table, cfg) {
    this.structures_config = JSON.parse(fs.readFileSync(path.resolve(__dirname, "structures/config.json")));
    this.cfg = cfg;
      
    this.table = table;

  }

  async start_game() {
    await this.units.initial();
  }

  connect(socket) {
    this.socket = socket;

    let this_class = this;

    let table = this.table;
    socket.on("world.ui-request_player_data", async function(options) {
      try {
        if (!socket.player) {
          let existing = await table.select("*", "user_id = $1", [socket.user_id]);
          if (existing[0]) {
            socket.player = this_class;
            
            existing[0].units = await this_class.units.table.select("*", "owner = $1", [existing[0].id]);
            existing[0].structures = await this_class.structures.table.select("*", "owner = $1", [existing[0].id]);
            existing[0].constructions = await this_class.constructions.table.select("*", "owner = $1", [existing[0].id]);
            existing[0].struct_config = this_class.constructions.config;

            socket.emit('world.io-player_data', {
              player : existing[0]
            });
          } else {
            socket.emit('world.io-player_data', {
              init : true
            });
          }
        } else {
          socket.emit('world.io-player_data', socket.player.cfg);
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    socket.on("world.ui-request_init_player", async function(name) {
      try {
        console.log("INIT PLAYER");
        let existing = await table.select("*", "name = $1", [name]);
        if (existing[0]) {
          socket.emit("world.io-init_player", {
            err: "Player name already taken!"
          });
        } else {
          await table.insert({
            name: name,
            user_id: socket.user_id
          });

          this_class.cfg = (await this_class.table.select("*", "user_id = $1", [socket.user_id]))[0];

          socket.player = this_class;
          await this_class.start_game();
          socket.emit("world.io-init_player", {
            msg: "success"
          });
        }
      } catch (e) {
        console.error(e.stack);
      };
    });

    socket.on("world.ui-request", function(what) {
      switch (what) {
        case 'time':
          socket.emit('world.io-response', {
            what : 'time',
            time : Date.now()
          });
          break;
        default:
          console.log("Invalid request: ", what);
      }
    });

    socket.on("world.ui-environment", async function(data) {
      try {
        let res_data = {
          rx: data.rx,
          ry: data.ry,
          zx: data.zx,
          zy: data.zy,
          radius: data.radius
        }
        
        res_data.env = await WORLD.environment.get_zone(data.rx, data.ry, data.zx, data.zy, data.radius);
        
        console.log("done!");
        socket.emit("world.io-environment", res_data);
      } catch (e) {
        console.error(e.stack);
      }
    });

    socket.on("world.ui-command", async function(data) {
      try {
        switch(data.command) {
          case "unit_move":
            await this_class.units.move(data);
            break;
          case "unit_gather":
            let resu = await this_class.units.move(data);
            setTimeout(async() => {
              await this_class.units.verify_command(data);
            }, resu.dest_time-resu.start_time);

            break;
          case "unit_bringres":
            let unit_bringres = await this_class.units.move(data);
            setTimeout(async() => {
              await this_class.units.verify_command(data);
            }, unit_bringres.dest_time-unit_bringres.start_time);

            break;
          case "unit_drop":
            await this_class.units.drop(data);
            break;
          case "unit_construct":
            let unit_construct = await this_class.units.move(data);
            setTimeout(async() => {
              await this_class.units.verify_command(data);
            }, unit_construct.dest_time-unit_construct.start_time);

            break;
          case "unit_store_res":
            let unit_store_res = await this_class.units.move(data);
            setTimeout(async() => {
              await this_class.units.verify_command(data);
            }, unit_store_res.dest_time-unit_store_res.start_time);
            break;
          case "build":
            let res_data = await this_class.constructions.add(data);
            res_data.command = data.command;
            socket.emit("world.io-command_response", res_data);
            break;
          case "destroy":
            if (data.type == "construction") { 
              let destroy_data = await this_class.constructions.remove(data);
              socket.emit("world.io-command_response", destroy_data);
            }
            break;
          default:
            console.error(new Error("Unknown wolrd.ui-command : "+data.command));
        }
      } catch (e) {
        console.error(e.stack);
      }
    });
  }
}
