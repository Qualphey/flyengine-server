
const WORLD = global.world = new (require("./World.js"))();
const Players = require("./players/index.js");

const Environment = require("./environment/index.js");

module.exports = class {
  constructor(players) {
    this.players = players;
  }

  static async init(aura, app_path) {
    await WORLD.construct(aura, app_path);
    WORLD.environment = await Environment.construct();
    let players = await Players.init(aura);
    
    return new module.exports(players);
  }
}
